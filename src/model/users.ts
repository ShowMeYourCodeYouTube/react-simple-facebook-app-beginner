export interface UsersResponse {
  results: User[]
  info: UsersInfo
}

export interface FilteredUser {
  isCollapsed: boolean
  user: User
  id: number
}

export interface UsersInfo {
  seed: string
  results: number
  page: number
  version: string
}

// The interface does not contain all values returned by an API.
export interface User {
  name: {
    title: string
    first: string
    last: string
  }
  location: {
    street: {
      name: string
      number: number
    }
    city: string
    country: string
    state: string
    postcode: number
  }
  email: string
  picture: {
    large: string
    medium: string
    thumbnail: string
  }
}
