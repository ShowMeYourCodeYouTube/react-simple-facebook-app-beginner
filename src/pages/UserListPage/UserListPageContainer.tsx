import React, { Component } from 'react'
import {
  Alert,
  Card,
  CardBody,
  Collapse,
  Form,
  Input,
  InputGroup,
  ListGroup,
  ListGroupItem,
} from 'reactstrap'
import { UserDetailsComponent } from './components/UserDetailsComponent/UserDetailsComponent'
import { type FilteredUser, type User } from '../../model/users'

interface UserListComponentProp {
  users: User[]
  isErrorDuringLoading: boolean
}

interface UserListComponentState {
  users: FilteredUser[]
  filteredUsers: FilteredUser[]
}

export class UserListPageContainer extends Component<
  UserListComponentProp,
  UserListComponentState
> {
  constructor(props: UserListComponentProp) {
    super(props)
    const users = this.mapUsersToListOfUsers(props.users)
    this.state = {
      users,
      filteredUsers: users,
    }
  }

  render(): React.ReactNode {
    return (
      <React.Fragment>
        <h1>Users</h1>
        <Form className='element-m-spacing-v'>
          <InputGroup>
            <Input
              placeholder='Search user...'
              onChange={($event: React.ChangeEvent<HTMLInputElement>) => {
                this.filterUsers($event.target.value)
              }}
            />
          </InputGroup>
        </Form>
        {this.renderErrorMessageIfNecessary(this.props.isErrorDuringLoading)}
        <ListGroup>{this.generateUsersList()}</ListGroup>
      </React.Fragment>
    )
  }

  private mapUsersToListOfUsers(users: User[]): FilteredUser[] {
    return users.map(function (user, index) {
      return {
        isCollapsed: false,
        user,
        id: index,
      }
    })
  }

  private prepareUserTitleForList(user: User): string {
    return `${user.name.first} ${user.name.last}`
  }

  private toggleUser(id: number): void {
    this.setState({
      ...this.state,
      users: this.state.users.map((elem: FilteredUser, index: number) => {
        if (id === elem.id) {
          elem.isCollapsed = !elem.isCollapsed
        }
        return elem
      }),
    })
  }

  private filterUsers(inputValue: string): void {
    if (this.state.users.length > 0) {
      this.setState({
        ...this.state,
        filteredUsers: this.state.users.filter((e: FilteredUser) =>
          this.prepareUserTitleForList(e.user).includes(inputValue),
        ),
      })
    }
  }

  private renderErrorMessageIfNecessary(isErrorDuringLoading: boolean): JSX.Element | null {
    return isErrorDuringLoading ? (
      <Alert color='danger'>Error during loading users! Please refresh the page.</Alert>
    ) : null
  }

  private generateUsersList(): JSX.Element[] {
    return this.state.filteredUsers.length > 0
      ? this.state.filteredUsers.map((e: FilteredUser, key: number) => (
          <React.Fragment key={key}>
            <ListGroupItem
              className='pointer'
              action
              onClick={() => {
                this.toggleUser(e.id)
              }}
            >
              {this.prepareUserTitleForList(e.user)}
            </ListGroupItem>
            <Collapse isOpen={e.isCollapsed}>
              <Card>
                <CardBody>
                  <UserDetailsComponent user={e.user} />
                </CardBody>
              </Card>
            </Collapse>
          </React.Fragment>
        ))
      : [
          <Alert color='warning' key='no-users'>
            No available users
          </Alert>,
        ]
  }
}
