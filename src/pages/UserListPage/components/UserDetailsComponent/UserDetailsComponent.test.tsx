import { render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { screen } from '@testing-library/dom'
import { UserDetailsComponent } from './UserDetailsComponent'

describe('UserDetailsComponent', () => {
  test('it should render a user', async () => {
    const user = {
      name: {
        title: 'Madame',
        first: 'Pia',
        last: 'Pierre',
      },
      location: {
        street: {
          number: 7261,
          name: 'Avenue des Ternes',
        },
        country: 'France',
        city: 'Fontenais',
        state: 'Luzern',
        postcode: 1072,
      },
      email: 'pia.pierre@example.com',
      picture: {
        large: 'https://randomuser.me/api/portraits/women/72.jpg',
        medium: 'https://randomuser.me/api/portraits/med/women/72.jpg',
        thumbnail: 'https://randomuser.me/api/portraits/thumb/women/72.jpg',
      },
    }
    render(<UserDetailsComponent user={user} />)

    expect(await screen.findByText('Email: pia.pierre@example.com')).toBeVisible()
  })
})
