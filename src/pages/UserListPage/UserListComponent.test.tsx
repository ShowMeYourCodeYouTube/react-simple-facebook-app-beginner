import { fireEvent, render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import React from 'react'
import { screen } from '@testing-library/dom'
import { type User } from '../../model/users'
import { UserListPageContainer } from './UserListPageContainer'

describe('UserListPageContainer', () => {
  test('it should mount', async () => {
    render(<UserListPageContainer users={[]} isErrorDuringLoading={false} />)

    expect(await screen.findByText('Users')).toBeVisible()
  })

  test('it should show an error message', async () => {
    render(<UserListPageContainer users={[]} isErrorDuringLoading={true} />)

    expect(await screen.findByText('No available users')).toBeVisible()
  })

  test('it should render users as list', async () => {
    const users: User[] = [
      {
        name: {
          title: 'Madame',
          first: 'Pia',
          last: 'Pierre',
        },
        location: {
          street: {
            number: 7261,
            name: 'Avenue des Ternes',
          },
          country: 'France',
          city: 'Fontenais',
          state: 'Luzern',
          postcode: 1072,
        },
        email: 'pia.pierre@example.com',
        picture: {
          large: 'https://randomuser.me/api/portraits/women/72.jpg',
          medium: 'https://randomuser.me/api/portraits/med/women/72.jpg',
          thumbnail: 'https://randomuser.me/api/portraits/thumb/women/72.jpg',
        },
      },
      {
        name: {
          title: 'Mrs',
          first: 'Tilde',
          last: 'Andersen',
        },
        location: {
          street: {
            number: 9422,
            name: 'Runddyssen',
          },
          country: 'Denmark',
          city: 'Branderup J',
          state: 'Syddanmark',
          postcode: 70229,
        },
        email: 'tilde.andersen@example.com',
        picture: {
          large: 'https://randomuser.me/api/portraits/women/56.jpg',
          medium: 'https://randomuser.me/api/portraits/med/women/56.jpg',
          thumbnail: 'https://randomuser.me/api/portraits/thumb/women/56.jpg',
        },
      },
    ]
    const page = render(<UserListPageContainer users={users} isErrorDuringLoading={false} />)

    const elements = page.container.getElementsByClassName('list-group-item')

    expect(elements.length).toBe(2)
    expect(elements[0].textContent?.indexOf('Pia Pierre') === 0).toBeTruthy()
    expect(elements[1].textContent?.indexOf('Tilde Andersen') === 0).toBeTruthy()
  })

  test('it should filter users', async () => {
    const users: User[] = [
      {
        name: {
          title: 'Madame',
          first: 'Pia',
          last: 'Pierre',
        },
        location: {
          street: {
            number: 7261,
            name: 'Avenue des Ternes',
          },
          country: 'Switzerland',
          city: 'Fontenais',
          state: 'Luzern',
          postcode: 1072,
        },
        email: 'pia.pierre@example.com',
        picture: {
          large: 'https://randomuser.me/api/portraits/women/72.jpg',
          medium: 'https://randomuser.me/api/portraits/med/women/72.jpg',
          thumbnail: 'https://randomuser.me/api/portraits/thumb/women/72.jpg',
        },
      },
      {
        name: {
          title: 'Mrs',
          first: 'Tilde',
          last: 'Andersen',
        },
        location: {
          street: {
            number: 9422,
            name: 'Runddyssen',
          },
          country: 'Denmark',
          city: 'Branderup J',
          state: 'Syddanmark',
          postcode: 70229,
        },
        email: 'tilde.andersen@example.com',
        picture: {
          large: 'https://randomuser.me/api/portraits/women/56.jpg',
          medium: 'https://randomuser.me/api/portraits/med/women/56.jpg',
          thumbnail: 'https://randomuser.me/api/portraits/thumb/women/56.jpg',
        },
      },
    ]
    const page = render(<UserListPageContainer users={users} isErrorDuringLoading={false} />)

    const input = screen.getByPlaceholderText('Search user...')
    fireEvent.change(input, { target: { value: 'Tilde' } })
    expect((input as HTMLInputElement).value).toBe('Tilde')

    const elements = page.container.getElementsByClassName('list-group-item')
    expect(elements.length).toBe(1)
    expect(elements[0].textContent?.indexOf('Tilde Andersen') === 0).toBeTruthy()
  })
})
