import React from 'react'
import './NotFoundPage.scss'
import { ElementId } from '../../model/elementId'

const NotFoundPage: React.FC = () => (
  <div id={ElementId.NOT_FOUND} className='txt-alg-center'>
    <h1>404</h1>
    <p>The page does not exist.</p>
  </div>
)

export default NotFoundPage
