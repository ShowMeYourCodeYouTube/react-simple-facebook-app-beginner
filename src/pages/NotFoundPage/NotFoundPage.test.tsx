import React from 'react'
import { render } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import NotFoundPage from './NotFoundPage'
import { ElementId } from '../../model/elementId'

describe('NotFoundPage', () => {
  test('it should mount', () => {
    const page = render(<NotFoundPage />)

    const element = page.container.querySelector(`#${ElementId.NOT_FOUND}`)

    expect(element).toBeInTheDocument()
  })
})
