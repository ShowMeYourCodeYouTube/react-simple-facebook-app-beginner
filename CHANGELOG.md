# Changelog

## [3.1.0](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/compare/3.0.0...3.1.0) (2023-07-29)


### Features

* display a user's country ([0d0e171](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/0d0e171522443eaa12d5dd2873835443ca1bd9a9))
* redirect to NOT_FOUND page when routing is not configured ([0e7df22](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/0e7df2250305c6be625e1e139a389df9dd531c57))
* use a loading spinner while fetching data ([7a8779a](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/7a8779a47e7be9e2957ce8f0d5c645a7f787b87d))
* use the `loglevel` library instead of console.log ([22d4c94](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/22d4c94a87b421e97db600fe108bf6febf1e0556))


### Documentation

* update CONTRIBUTING.md ([486c6af](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/486c6afc10b582eb9e8ffd87066f6f3a6ba24a19))


### Other changes

* add SonarQube integration ([346da10](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/346da1085fe626c270d308b1da104f3ecb92a7ee))
* change routing paths to more intuitive names ([519f428](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/519f428040b0bd42fc010677245747a0265a55fc))

## [3.0.0](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/compare/book-of-people-2.1.0...3.0.0) (2023-06-09)


### Documentation

* add contributing guidelines ([f49510c](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/f49510c64aa1750a3c007317fee5d67bad69f2b4))
* add LICENSE ([e4cf2ba](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/e4cf2bad6cbdcc4a96316a25528480960eb39c49))
* update CONTRIBUTING.md ([7b48ee4](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/7b48ee4bed05b9db065778e5ca70203c84b1a02d))


### Other changes

* add a Netlify config file ([bb488d3](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/bb488d3f067c288a6ebdc92f260dfbd36264854c))
* add Husky ([b3f1575](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/b3f1575c5f5be40b5e75a07a091c10584bf89f78))
* add Release It ([bf6f6b7](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/bf6f6b7b3e6d7ebba4cb488e6a3dc4b640ef6872))
* adjust the changelog structure based on conventional commits ([65b2253](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/65b2253ada5dfa4e2285b7d7ea2662cb1a516cd6))
* bump React to 18.2 ([7497563](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/7497563ebe6b710023142d05cbea6cc9374510a5))
* enforce code style with ESLint and Prettier ([bf0d87a](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/bf0d87aaafa4279dfe3bd6abd4db3d8d062d5372))
* publish a test report using Gitlab Pages ([36892fe](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/36892fe331ceb3b3278cc1404e7166b1a156a476))
* rename InitPage to HomePage ([78bbe49](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/78bbe497b0db8047b536bd93a6a4b1eb3ed81b41))
* update browserslist ([bb0916c](https://gitlab.com/ShowMeYourCodeYouTube/react-simple-app/commit/bb0916c1eb77fb189f6cf07d7f7f58ebf69ac6fc))