# Reactjs - a simple app for beginners

- Live demo: https://react-simple-app-beginner.netlify.app/  
- Test report: https://showmeyourcodeyoutube.gitlab.io/react-simple-app/unit-tests/index.html

---

![Online demo](online-demo.gif)

- ReactJS tutorial - How to create first app? Course for beginners. [ENG]
    - https://www.youtube.com/watch?v=k5uVrwpW1tA&list=PLRkaFIl3mgdXYPTZ2nUiJKNY30wV7ZxZn&ab_channel=ShowMeYourCode%21
- How to add Typescript to existing ReactJS project? Step-by-step tutorial [ENG]
    - https://www.youtube.com/watch?v=A_rrMXLwyqI
- Generate React component using CLI [ENG]
    - https://youtu.be/aCoHxxrbVIY

## Technology stack

- React
- Typescript (initially a project was bootstrapped without Typescript, then it was migrated)
- Reactstrap
- Axios (http library)
- Testing Library & Jest
  - Jest is a test runner that finds tests, runs the tests, and determines whether the tests passed or failed. Additionally, Jest offers functions for test suites, test cases, and assertions.
  - React Testing Library provides virtual DOMs for testing React components.

## Local development

### Project setup

1. Go to the root directory
2. Install dependencies using a command ``npm install``
3. Start the application using a command ``npm run start``

### Generating components

Ref: https://www.npmjs.com/package/generate-react-cli  

```
npx generate-react-cli component Box
```

### Known issues

- componentDidMount called twice for initial load
  - It's just because we do 2 initial renders in dev mode to avoid getting warnings about mismatched client/server markup when we introduce the devtools. Doesn't affect production.
  - Ref: https://github.com/erikras/react-redux-universal-hot-example/issues/429#issuecomment-151990676


## SonarQube

https://docs.sonarqube.org/latest/user-guide/metric-definitions/
